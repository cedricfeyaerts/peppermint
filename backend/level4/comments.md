# Comments

As my code it might not be as self-explanatory as I wish it to be, I will explain
here my process.

## Microservices

You don't see it but my first reaction was to create microservices to remove the
calculation from the model. I backtracked because it creates a lot of complexity for
a simple task like this one. I'm also to be honest not as familiar with that pattern as
I would like to be.

## Exceptions

I added some error. It is past due. I think I only need to raise them not to catch
them as I want the script to fail if something goes amiss.

## reduction_coefficient

Not optimal. I considered a little class `StepedScale` or something of the like.
The implementation seemed a bit extravagant considering I would only use it once.

## Minitest here I come

Those tests are simplist, all in the file. My purpose here was to spend the least time
possible while keeping them readable.
I have to adnmit it's still an area for personal improvement.
