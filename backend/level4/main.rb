# frozen_string_literal: true

require 'JSON'
require 'Date'

INPUT_PATH = 'data/input.json'
OUTPUT_PATH = 'data/output.json'

class Car
  attr_reader :id, :price_per_day, :price_per_km

  def self.save(object)
    @indices ||= {}
    @indices[object.id] = object
  end

  def self.find(id)
    @indices[id]
  end

  def initialize(hash)
    raise ArgumentError, 'Missing argument' unless hash['id'] && hash['price_per_day'] && hash['price_per_km']

    @id = hash['id']
    @price_per_day = hash['price_per_day']
    @price_per_km = hash['price_per_km']

    raise ArgumentError, 'Inconsistent amount' if price_per_day.negative? || price_per_km.negative?

    self.class.save(self)
  end
end

class Rental
  INSURANCE_COEF = 0.5
  COMMISSION_COEF = 0.3
  ASSISTANCE_DAILY_PRICE = 100

  DAILY_REDUCTION_SCALE = [
    { min: 2, coef: 0.9 },
    { min: 5, coef: 0.7 },
    { min: 11, coef: 0.5 }
  ].freeze

  attr_reader :id, :car_id, :start_date, :end_date, :distance

  def initialize(hash)
    raise ArgumentError, 'Missing argument' unless hash['id'] && hash['car_id'] && hash['distance']

    @id = hash['id']
    @car_id = hash['car_id']
    @start_date = Date.parse(hash['start_date'])
    @end_date = Date.parse(hash['end_date'])
    @distance = hash['distance']

    raise ArgumentError, 'Unknown car' if car.nil?
    raise ArgumentError, 'Inconsistent dates' if @start_date > @end_date
    raise ArgumentError, 'Inconsistent amount' if drivy_fee.negative?
    raise ArgumentError, 'Inconsistent distance' if distance.negative?
  end

  def car
    Car.find(car_id)
  end

  def duration
    end_date - start_date.prev_day
  end

  def price
    duration_price + distance_price
  end

  def driver_fee
    price * -1
  end

  def insurance_fee
    (price * COMMISSION_COEF * INSURANCE_COEF).ceil
  end

  def assistance_fee
    (duration * ASSISTANCE_DAILY_PRICE).ceil
  end

  def drivy_fee
    commission - insurance_fee - assistance_fee
  end

  def owner_fee
    price - commission
  end

  def actions
    [
      Action.new(who: Action::DRIVER, amount: driver_fee),
      Action.new(who: Action::OWNER, amount: owner_fee),
      Action.new(who: Action::INSURANCE, amount: insurance_fee),
      Action.new(who: Action::ASSISTANCE, amount: assistance_fee),
      Action.new(who: Action::DRIVY, amount: drivy_fee)
    ]
  end

  def as_json
    {
      id: id,
      actions: actions.map(&:as_json)
    }
  end

  private

  def reduction_coefficient(day)
    DAILY_REDUCTION_SCALE.sort { |s| s[:min] }.each do |step|
      return step[:coef] if day >= step[:min]
    end

    1
  end

  def duration_price
    sum = 0

    (1..duration.ceil).each do |day|
      sum += car.price_per_day * reduction_coefficient(day)
    end
    sum.ceil
  end

  def distance_price
    distance * car.price_per_km
  end

  def commission
    (price * COMMISSION_COEF).round
  end
end

class Action
  attr_reader :who

  DRIVER = 'driver'
  OWNER = 'owner'
  INSURANCE = 'insurance'
  ASSISTANCE = 'assistance'
  DRIVY = 'drivy'

  DEBIT = 'debit'
  CREDIT = 'credit'

  def initialize(who: nil, amount: nil)
    raise ArgumentError, 'Missing argument' unless who && amount
    raise ArgumentError, 'Unknown who' unless [DRIVER, OWNER, INSURANCE, ASSISTANCE, DRIVY].include? who

    @who = who
    @amount = amount
  end

  def type
    return DEBIT if @amount.negative?

    CREDIT
  end

  def amount
    @amount.abs
  end

  def as_json
    {
      who: who,
      type: type,
      amount: amount
    }
  end
end

data = JSON.parse File.read(INPUT_PATH)

data['cars'].each { |c| Car.new(c) }
rentals = data['rentals'].map { |r| Rental.new(r) }
output = { rentals: [] }

rentals.each do |rental|
  output[:rentals] << rental.as_json
end

File.write(OUTPUT_PATH, "#{JSON.pretty_generate(output)}\n")
