# frozen_string_literal: true

require 'JSON'
require 'Date'

INPUT_PATH = 'data/input.json'
OUTPUT_PATH = 'data/output.json'

class Car
  attr_reader :id, :price_per_day, :price_per_km

  def self.save(object)
    @indices ||= {}
    @indices[object.id] = object
  end

  def self.find(id)
    @indices[id]
  end

  def initialize(hash)
    @id = hash['id']
    @price_per_day = hash['price_per_day']
    @price_per_km = hash['price_per_km']
    self.class.save(self)
  end
end

class Rental
  attr_reader :id, :car_id, :start_date, :end_date, :distance

  INSURANCE_COEF = 0.5
  COMMISSION_COEF = 0.3
  ASSISTANCE_DAILY_PRICE = 100

  def initialize(hash)
    @id = hash['id']
    @car_id = hash['car_id']
    @start_date = Date.parse(hash['start_date'])
    @end_date = Date.parse(hash['end_date'])
    @distance = hash['distance']
  end

  def car
    Car.find(car_id)
  end

  def duration
    end_date - start_date.prev_day
  end

  def duration_price
    sum = 0

    duration.ceil.times do |day|
      sum += car.price_per_day * reduction_coefficient(day + 1)
    end
    sum.ceil
  end

  def reduction_coefficient(day)
    case day
    when 1
      1
    when (2..4)
      0.9
    when (5..10)
      0.7
    else
      0.5
    end
  end

  def distance_price
    distance * car.price_per_km
  end

  def price
    duration_price + distance_price
  end

  def commission
    (price * COMMISSION_COEF).round
  end

  def insurance_fee
    (price * COMMISSION_COEF * INSURANCE_COEF).ceil
  end

  def assistance_fee
    (duration * ASSISTANCE_DAILY_PRICE).ceil
  end

  def drivy_fee
    commission - insurance_fee - assistance_fee
  end

  def output
    {
      id: id,
      price: price,
      commission: {
        insurance_fee: insurance_fee,
        assistance_fee: assistance_fee,
        drivy_fee: drivy_fee
      }
    }
  end
end

data = JSON.parse File.read(INPUT_PATH)

data['cars'].each { |c| Car.new(c) }
rentals = data['rentals'].map { |r| Rental.new(r) }
output = { rentals: [] }

rentals.each do |rental|
  output[:rentals] << rental.output
end

File.write(OUTPUT_PATH, "#{JSON.pretty_generate(output)}\n")
