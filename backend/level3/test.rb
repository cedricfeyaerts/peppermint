# frozen_string_literal: true

require 'minitest/autorun'

load 'main.rb'

class TestRental < Minitest::Test
  def setup
    @car = Car.new('id' => 1, 'price_per_day' => 1000, 'price_per_km' => 10)
  end

  def test_price_degressivity
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-01', 'distance' => 100).price, 2000
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-02', 'distance' => 100).price, 2900
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-06', 'distance' => 100).price, 6100
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-12', 'distance' => 100).price, 9900
  end

  def test_insurance_fee
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-01', 'distance' => 200).insurance_fee, 450
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-02', 'distance' => 100).insurance_fee, 435
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-06', 'distance' => 10).insurance_fee, 780
  end

  def test_assistance_fee
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-01', 'distance' => 200).assistance_fee, 100
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-02', 'distance' => 100).assistance_fee, 200
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-06', 'distance' => 10).assistance_fee, 600
  end

  def test_drivy_fee
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-01', 'distance' => 200).drivy_fee, 350
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-02', 'distance' => 100).drivy_fee, 235
    assert_equal Rental.new('id' => 1, 'car_id' => 1, 'start_date' => '2018-01-01', 'end_date' => '2018-01-06', 'distance' => 10).drivy_fee, 180
  end
end

class TestIntegration < Minitest::Test
  def test_match_expected_output
    expected_output = File.read('data/expected_output.json')
    `ruby main.rb`
    output = File.read('data/output.json')

    assert_equal output, expected_output
  end
end
