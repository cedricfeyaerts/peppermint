# Comments

As my code might not be as self-explanatory as I wish it to be, I will explain
here my process.

## How I refactor

Basically, I keep the old data and the old test. I then refactor what bugs me in the old way.
When everything works as expected I then introduce the new data and test and make it match.
Usually, I would keep the old test as well but here the brief induce a breaking change.

## OOP

Who was I kidding? Of course, as the expected gain complexity, the need for oop arise.
The is still a lot to be done. It is not very DRY. There is some magic constant here and there.
We will be fixing that soon enough. I mean, I have to show you my process right?

## Why everything on the same file?

Well, there is only two model and they are pretty small at that. It is still a script and it is easier to a
global view if everything fits on the same screen.

## What about `reduction_coefficient`

I know I'm not proud of that one. It's not very clean because of constants hidden in the code and
it is very specific.
On the pro side, it is very readable. If I generalize it will definitely lose its readability.
I left it there because sometimes the better idea comes when the need arises (maybe level 3 or 4?).

## Store data

I still store the car and rental collection in global variables. Obviously, for a bigger set I would need some
better storage like a database. I don't like to repeat the car argument all the time, I will find something to
access it easier.

## Useless attribute reader

Like the `rentad.id`, I never used it why defining it? For symmetry. I think it helps a lot for the readability
if the code is symmetric. `Car` has an id and all the elements from the yaml are in the model. So rental will
have it too.
