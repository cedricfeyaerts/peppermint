# frozen_string_literal: true

require 'JSON'
require 'Date'

INPUT_PATH = 'data/input.json'
OUTPUT_PATH = 'data/output.json'

class Car
  attr_reader :id, :price_per_day, :price_per_km

  def initialize(hash)
    @id = hash['id']
    @price_per_day = hash['price_per_day']
    @price_per_km = hash['price_per_km']
  end
end

class Rental
  attr_reader :id, :car_id, :start_date, :end_date, :distance

  def initialize(hash)
    @id = hash['id']
    @car_id = hash['car_id']
    @start_date = Date.parse(hash['start_date'])
    @end_date = Date.parse(hash['end_date'])
    @distance = hash['distance']
  end

  def duration
    end_date - start_date.prev_day
  end

  def duration_price(car)
    sum = 0

    duration.ceil.times do |day|
      sum += car.price_per_day * reduction_coefficient(day + 1)
    end
    sum.ceil
  end

  def reduction_coefficient(day)
    case day
    when 1
      1
    when (2..4)
      0.9
    when (5..10)
      0.7
    else
      0.5
    end
  end

  def distance_price(car)
    distance * car.price_per_km
  end

  def price(car)
    duration_price(car) + distance_price(car)
  end

  def output(car)
    {
      id: id,
      price: price(car)
    }
  end
end

data = JSON.parse File.read(INPUT_PATH)

cars = Hash[data['cars'].map { |c| [c['id'], Car.new(c)] }]
rentals = data['rentals'].map { |r| Rental.new(r) }
output = { rentals: [] }

rentals.each do |rental|
  car = cars[rental.car_id]
  output[:rentals] << rental.output(car)
end

File.write(OUTPUT_PATH, "#{JSON.pretty_generate(output)}\n")
