# Comments

As my code might not be as self-explanatory as I wish it to be, I will explain
here my process.

## Why not OOP?

Because of its simple scope, this screams 'script' to me. I don't think it's always best to
throw yourself in complex applications for a task you might just use a couple of time.
Here is an example of good old throwable code.

## First reflex rubocop

Let's face it some people are going to peak over my shoulder on this one. Common rules are a must
in big teams especially in decentralized one.
I have to says I disabled some of the rules, the metrics really bugs me. I might have something
to work on, there

## The test

I can barely call that a test at all. 'ruby test.rb' only verify if the output matches the expected.
