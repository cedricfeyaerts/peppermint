# frozen_string_literal: true

require 'JSON'
require 'Date'

data = JSON.parse File.read('data/input.json')

cars = Hash[data['cars'].map { |c| [c['id'], c] }]
rentals = data['rentals']

rentals_output = rentals.map do |rental|
  car = cars[rental['car_id']]
  distance_price = rental['distance'] * car['price_per_km']
  duration = Date.parse(rental['end_date']) - Date.parse(rental['start_date']).prev_day
  duration_price = duration.ceil * car['price_per_day']

  {
    id: rental['id'],
    price: distance_price + duration_price
  }
end

File.write('data/output.json', "#{JSON.pretty_generate(rentals: rentals_output)}\n")
