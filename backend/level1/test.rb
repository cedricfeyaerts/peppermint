# frozen_string_literal: true

expected_output = File.read('data/expected_output.json')
`ruby main.rb`
output = File.read('data/output.json')

abort("Output doesn't match expected_output") unless output == expected_output

puts 'OK'
