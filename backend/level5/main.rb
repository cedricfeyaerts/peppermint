# frozen_string_literal: true

require './lib/main.rb'

INPUT_PATH = 'data/input.json'
OUTPUT_PATH = 'data/output.json'

main = Main.new INPUT_PATH
File.write(OUTPUT_PATH, "#{main.knitpicked_print}\n")
