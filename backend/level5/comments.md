# Comments

As my code might not be as self-explanatory as I wish it to be, I will explain
here my process.

## Knitpicking

The pretty_print from JSON doesn't quiet fit the bill. My guess would be the
formater isn't very important as pretty printed json is not the best to
transfer data. But just in case here knitpicked_print are there but it's just
a quick fix.

## Test

Yes, there is a cruel lack of test. I sacrificed it in the name of time.

## Difficulties

I spent more time than announced. A lot more. It is partially due to the fact
I rarely do pure ruby. I'm more of a Rails developer and habits die hard.
Well, I define myself as full stack but that's another story. Not much js I'm afraid.
I also had to backtrack from some wrong solution.

## What I would still do

* The errors could still gain in clarity.
* Test obviously.
* I think the way the actions are created could be more efficient. It feels
artificial somehow. Maybe an ActionManager to isolate the complex calculations.
It would take rental in argument and spit the required actions.
* Options and action are created on the fly but they should store of course (db etc)
* Here a lot of the tarifs are in constants but they should be in the database as well
the financial fine-tuning should be available via admin of some sort.

## Timezone

It bugged me since the beginning but in the data, there is no timezone. I mean that
could create some fun bugs.

## Final words

Thank you for reading me till here. In any case (whether you chose to pursue or not)
it has been interesting. To step back and reconsider my way of coding, to leave rails' crutches for a little while
but also to learn to manage that crippling sensation when someone peeks over my shoulder :-)
