# frozen_string_literal: true

require 'JSON'
require 'Date'
require './models/concerns/findable.rb'
require './models/action.rb'
require './models/car.rb'
require './models/option.rb'
require './models/rental.rb'

class Main
  def initialize(input_path)
    raise ArgumentError, 'Missing argument' unless input_path

    data = JSON.parse File.read(input_path)
    Car.data = data['cars']
    Rental.data = data['rentals']
    Option.data = data['options']
  end

  def output
    @output = { rentals: [] }
    Rental.each do |rental|
      @output[:rentals] << rental.as_json
    end

    @output
  end

  def pretty_print
    JSON.pretty_unparse(output)
  end

  def knitpicked_print
    pretty_print.gsub(/\[[ \n]*\]/, '[]')
  end
end
