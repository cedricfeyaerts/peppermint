# frozen_string_literal: true

module ActionManager
  INSURANCE_COEF = 0.5
  COMMISSION_COEF = 0.3
  ASSISTANCE_DAILY_PRICE = 100

  DAILY_REDUCTION_SCALE = [
    { min: 2, coef: 0.9 },
    { min: 5, coef: 0.7 },
    { min: 11, coef: 0.5 }
  ].freeze

  attr_reader :rental

  def initialize(rental)
    @rental = rental
  end

  def driver_fee
    price * -1
  end

  def insurance_fee
    (price * COMMISSION_COEF * INSURANCE_COEF).ceil
  end

  def assistance_fee
    (rental.duration * ASSISTANCE_DAILY_PRICE).ceil
  end

  def drivy_fee
    commission - insurance_fee - assistance_fee
  end

  def owner_fee
    price - commission
  end

  private

  def reduction_coefficient(day)
    DAILY_REDUCTION_SCALE.sort { |s| s[:min] }.each do |step|
      return step[:coef] if day >= step[:min]
    end

    1
  end

  def price
    distance_price = rental.distance * rental.car.price_per_km

    duration_price = (1..rental.duration.ceil).map do |day|
      rental.car.price_per_day * reduction_coefficient(day)
    end.inject(&:+).ceil

    duration_price + distance_price
  end

  def commission
    (price * COMMISSION_COEF).round
  end
end
