# frozen_string_literal: true

class Option
  extend Findable

  OPTIONS = {
    'gps' => [
      { who: Action::DRIVER, amount: -500 },
      { who: Action::OWNER, amount: 500 }
    ],
    'baby_seat' => [
      { who: Action::DRIVER, amount: -200 },
      { who: Action::OWNER, amount: 200 }
    ],
    'additional_insurance' => [
      { who: Action::DRIVER, amount: -1000 },
      { who: Action::DRIVY, amount: 1000 }
    ]
  }.freeze

  attr_reader :id, :rental_id, :type

  def initialize(hash)
    raise ArgumentError, 'Missing argument' unless hash['id'] && hash['rental_id'] && hash['type']
    raise ArgumentError, 'Unknown type' unless OPTIONS.key? hash['type']

    @id = hash['id']
    @rental_id = hash['rental_id']
    @type = hash['type']

    rental.options << self
  end

  def actions
    OPTIONS[type].map do |action_hash|
      Action.new(who: action_hash[:who], amount: daily_fee(action_hash[:amount]))
    end
  end

  def daily_fee(fee)
    (fee * rental.duration).ceil
  end

  def rental
    Rental.find(rental_id)
  end
end
