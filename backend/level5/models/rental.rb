# frozen_string_literal: true

class Rental
  extend Findable

  INSURANCE_COEF = 0.5
  COMMISSION_COEF = 0.3
  ASSISTANCE_DAILY_PRICE = 100

  DAILY_REDUCTION_SCALE = [
    { min: 2, coef: 0.9 },
    { min: 5, coef: 0.7 },
    { min: 11, coef: 0.5 }
  ].freeze

  attr_reader :id, :car_id, :start_date, :end_date, :distance
  attr_accessor :options

  def initialize(hash)
    raise ArgumentError, 'Missing argument' unless hash['id'] && hash['car_id'] && hash['distance']

    @start_date = Date.parse(hash['start_date'])
    @end_date = Date.parse(hash['end_date'])
    @id = hash['id']
    @car_id = hash['car_id']
    @distance = hash['distance']
    @options = []

    raise ArgumentError, 'Inconsistent dates' if @start_date > @end_date
    raise ArgumentError, 'Inconsistent amount' if drivy_fee.negative?
    raise ArgumentError, 'Inconsistent distance' if distance.negative?

    self.class.save self
  end

  def car
    Car.find(car_id)
  end

  def duration
    end_date - start_date.prev_day
  end

  def price
    duration_price + distance_price
  end

  def driver_fee
    price * -1
  end

  def insurance_fee
    (price * COMMISSION_COEF * INSURANCE_COEF).ceil
  end

  def assistance_fee
    (duration * ASSISTANCE_DAILY_PRICE).ceil
  end

  def drivy_fee
    commission - insurance_fee - assistance_fee
  end

  def owner_fee
    price - commission
  end

  def actions
    actions = [
      Action.new(who: Action::DRIVER, amount: driver_fee),
      Action.new(who: Action::OWNER, amount: owner_fee),
      Action.new(who: Action::INSURANCE, amount: insurance_fee),
      Action.new(who: Action::ASSISTANCE, amount: assistance_fee),
      Action.new(who: Action::DRIVY, amount: drivy_fee)
    ]
    options.each do |option|
      actions += option.actions
    end

    actions
  end

  def as_json
    {
      id: id,
      options: options.map(&:type),
      actions: Action.agregate(actions).map(&:as_json)
    }
  end

  private

  def reduction_coefficient(day)
    DAILY_REDUCTION_SCALE.sort { |s| s[:min] }.each do |step|
      return step[:coef] if day >= step[:min]
    end

    1
  end

  def duration_price
    sum = 0

    (1..duration.ceil).each do |day|
      sum += car.price_per_day * reduction_coefficient(day)
    end
    sum.ceil
  end

  def distance_price
    distance * car.price_per_km
  end

  def commission
    (price * COMMISSION_COEF).round
  end
end
