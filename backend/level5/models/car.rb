# frozen_string_literal: true

class Car
  extend Findable

  attr_reader :id, :price_per_day, :price_per_km

  def initialize(hash)
    raise ArgumentError, 'Missing argument' unless hash['id'] && hash['price_per_day'] && hash['price_per_km']

    @id = hash['id']
    @price_per_day = hash['price_per_day']
    @price_per_km = hash['price_per_km']

    raise ArgumentError, 'Inconsistent amount' if price_per_day.negative? || price_per_km.negative?

    self.class.save self
  end
end
