# frozen_string_literal: true

class Action
  attr_reader :who

  DRIVER = 'driver'
  OWNER = 'owner'
  INSURANCE = 'insurance'
  ASSISTANCE = 'assistance'
  DRIVY = 'drivy'

  DEBIT = 'debit'
  CREDIT = 'credit'

  def initialize(who: nil, amount: nil)
    raise ArgumentError, 'Missing argument' unless who && amount
    raise ArgumentError, 'Unknown who' unless [DRIVER, OWNER, INSURANCE, ASSISTANCE, DRIVY].include? who

    @who = who
    @amount = amount
  end

  def type
    return DEBIT if @amount.negative?

    CREDIT
  end

  def amount
    @amount.abs
  end

  def real_amount
    @amount
  end

  def as_json
    {
      who: who,
      type: type,
      amount: amount
    }
  end

  def self.agregate(actions)
    actions.group_by(&:who).map do |who, action_agregate|
      amount = action_agregate.map(&:real_amount).inject(&:+)
      new who: who, amount: amount
    end
  end
end
