# frozen_string_literal: true

module Findable
  def save(object)
    @indices ||= {}
    @indices[object.id] = object
  end

  def find(id)
    raise ArgumentError, 'Id unknown' unless @indices[id]

    @indices[id]
  end

  def data=(data)
    @indices = {}
    data.each do |d|
      object = new(d)
      save(object)
    end
  end

  def each(*args, &block)
    @indices.values.each(*args, &block)
  end
end
